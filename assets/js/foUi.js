let hanaAiSearchUi = {}
$(() => {
    //1.상단 검색 삭제 버튼 실행
    hanaAiSearchUi.InputBtnDelete.init({limitNum:1, element:'.form-input-1-1', delEl: '.search-form-wrap .btn-del'});
    //2.아코디언 샐행
    hanaAiSearchUi.accordionUI.init({areaElement:'.search-result-2', element:'.acc-top-area .acc-btn-2', allBtns:'.all-acc-btn'});
})

/**
 *hanaAiSearchUi.InputBtnDelete: 검색 삭제 버튼
 *Author: JIY
 *usage:
 *hanaAiSearchUi.InputBtnDelete.init({
    limitNum:글자수|<Number>|1, 
    element:입력요소|String|'.form-input-1-1', 
    delEl:삭제버튼요소|String|'.search-form-wrap .btn-del'
})
*/
hanaAiSearchUi.InputBtnDelete = {
    init: function(options) {
        this.self = this;
        this.txtNum = 0;
        this.limit = options.limitNum;
        this.bind01 = this.eventHandler01.bind(this);
        this.bind02 = this.eventHandler02.bind(this);
        this.FormInpEl = $(options.element);
        $(document).on('keyup',options.element, this.bind01);
        $(document).on('click',options.delEl, this.bind02);
    },
    eventHandler01:function(evt) {
        const me = $(evt.target);
        let meVal = me.val();
        let meValLength = meVal.length;
        if(meValLength >= this.limit) {
            me.addClass('delete');
        } else {
            me.removeClass('delete');
        }
    },
    eventHandler02:function(evt) {
        const me = $(evt.target);
        this.FormInpEl.val('');
        this.FormInpEl.removeClass('delete');
        this.FormInpEl.focus();
    }
}
/**
 *hanaAiSearchUi.accordionUI: 아코디언
 *Author: JIY
 *usage:
 *hanaAiSearchUi.accordionUI.init({
     areaElement:아코디언 영역 선택자|String|'.search-result-2', 
     element:아코디언 열기닫기 버튼 선택자|String|'.acc-top-area .acc-btn-2', 
     allBtns:아코디언 전체열기닫기 버튼 선택자|String|'.all-acc-btn'
  });
*/
hanaAiSearchUi.accordionUI = {
    init:function(options) {
        hanaAiSearchUi.accordionUI.bind01 = this.eventHandler01.bind(this, options);
        hanaAiSearchUi.accordionUI.bind02 = this.eventHandler02.bind(this, options);
        $(document).on('click',options.element,this.bind01);
        $(document).on('change',options.allBtns,this.bind02);
    },
    eventHandler01:function(options, evt) {
        const me = $(evt.target);
        const accTopArea = me.closest('.acc-top-area');
        const accTopAreaAll = accTopArea.siblings().add(accTopArea);
        const accTopAreaAllLength = accTopAreaAll.length;
        const accTopAreaNextElement = accTopArea.next();
        const allBtns = $(options.areaElement + ' .all-acc-btn');
        if(accTopArea.hasClass('active')) {
            accTopAreaNextElement.stop(true).slideUp(400, function() {
                accTopArea.removeClass('active');
                if(accTopArea.closest(options.areaElement + ' .acc-list-1').find('.active').length === 0) {
                    allBtns.prop('checked', false)
                }
            })
        } else {
            accTopAreaNextElement.stop(true).slideDown(400,function() {
                accTopArea.addClass('active');
                if(accTopArea.closest(options.areaElement + ' .acc-list-1').find('.active').length === accTopAreaAllLength) {
                    allBtns.prop('checked', true)
                }
            })
        }
    },
    eventHandler02:function(options, evt) {
        const me = $(evt.target);
        const accTopArea = $(options.areaElement + ' .acc-top-area');
        const accTopAreaNext = accTopArea.next();
        if(me.is(':checked')) {
            accTopAreaNext.slideDown(400, function() {
                accTopArea.addClass('active');
            })
        } else {
            accTopAreaNext.slideUp(400, function() {
                accTopArea.removeClass('active');
            })
        }
    }
}
